import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes:Recipe[]=[
    new Recipe('Birayani', 'Special WestBEngal Biriayani','https://headstrongperformance.net/wp-content/uploads/2016/03/logo.jpg'),
    new Recipe('Birayani2', 'Special WestBEngal Birayani','https://headstrongperformance.net/wp-content/uploads/2016/03/logo.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
